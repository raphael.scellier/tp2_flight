<?php require "vendor/autoload.php";

Flight::route('/', function(){
    echo 'Hello World!';
});


Flight::route('/hello/@name', function($name){
    echo "<h2>Hello, $name!</h2>";
});

Flight::start();